mod {
  SHARED {
    # set correct label and flag for default language in BE
    defaultLanguageFlag = gb
    defaultLanguageLabel = English
  }

  # show translated records next to the original ones
  web_layout.defLangBinding = 1

  web_list.enableLocalizationView = activated
  web_list.enableDisplayBigControlPanel = activated
}

TCEMAIN {
  permissions.groupid = 2
  clearCacheCmd = pages
}

TCEFORM.pages {
  # storage pid for BE layouts
  _STORAGE_PID = 948

  layout {
    removeItems = 1,2,3
  }
}

TCEFORM.tt_content {
  section_frame {
    removeItems = 1,5,6,10,11,12,20,21
  }

  layout {
    removeItems = 2,3
    altLabels.1 = Slider
    addItems.51 = Carousel
  }

  header_layout {
    removeItems = 4,5
  }

  imageorient {
    # remove in-text image alignments
    removeItems = 17,18
  }

  tx_gridelements_backend_layout.PAGE_TSCONFIG_ID = 1
}
