TCEFORM {
  # remove default layouts
  tx_powermail_domain_model_forms.css {
    removeItems = layout1, layout2, layout3
  }

  tx_powermail_domain_model_pages.css < .tx_powermail_domain_model_forms.css
  tx_powermail_domain_model_fields.css < .tx_powermail_domain_model_forms.css
}
