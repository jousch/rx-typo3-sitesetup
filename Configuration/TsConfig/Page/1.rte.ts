# RTE config is based on the minimal-preset of the extension

RTE.default.FE >

RTE.default {
  contentCSS = EXT:sitesetup/Resources/Public/Css/rte.css

  showStatusBar = 1
  keepButtonGroupTogether = 1
  keepToggleBordersInToolbar = 1

  enableWordClean = 1
  removeTrailingBR = 1
  removeComments = 1
  removeTags = center, font, o:p, sdfield, strike, u
  removeTagsAndContents = link, meta, script, style, title

  ## Configuration specific to the TableOperations feature
  ## Remove the following fieldsets from the table operations dialogs
  disableAlignmentFieldsetInTableOperations = 1
  disableSpacingFieldsetInTableOperations = 1
  disableColorFieldsetInTableOperations = 1
  disableLayoutFieldsetInTableOperations = 1
  disableBordersFieldsetInTableOperations = 1

  showButtons := addToList(blockstylelabel, blockstyle, textstylelabel, textstyle)
  showButtons := addToList(formatblock, bold, italic, subscript, superscript)
  showButtons := addToList(line, left, center, right)
  showButtons := addToList(orderedlist, unorderedlist, textindicator)
  showButtons := addToList(pastetoggle, insertcharacter, link, table, findreplace, chMode, removeformat, undo, redo, about)
  showButtons := addToList(toggleborders, tableproperties)
  showButtons := addToList(rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit)
  showButtons := addToList(columninsertbefore, columninsertafter, columndelete, columnsplit)
  showButtons := addToList(cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge)
}

RTE.default.buttons {
  left.useClass = text-left
  right.useClass = text-right
  center.useClass = text-center

  bold.hotKey = b
  italic.hotKey = i

  toggleborders.setOnTableCreation = 1

  blockstyle.tags.div.allowedClasses = text-left, text-center, text-right
  blockstyle.tags.td.allowedClasses = text-left, text-center, text-right
  blockstyle.tags.table.allowedClasses = table, table-bordered, table-condensed
  blockstyle.showTagFreeClasses = 1
  blockstyle.disableStyleOnOptionLabel = 1

  formatblock.removeItems = h4, h5, h6, pre, address, article, aside, blockquote, div, footer, header, nav, section

  textstyle.tags.span.allowedClasses = small
}

RTE.default.proc {
  allowedClasses := addToList(table, table-bordered, table-condensed, text-left, text-right, text-center, small)
  entryHTMLparser_db.allowTags < .allowTags
  entryHTMLparser_db.allowedClasses < .allowedClasses
}

## Use same processing as on entry to database to clean content pasted into the editor
RTE.default.enableWordClean.HTMLparser < RTE.default.proc.entryHTMLparser_db

RTE.classes {
  small.name = LLL:EXT:sitesetup/Resources/Private/Language/locallang_rte.xlf:classes.small
  text-left.name = LLL:EXT:sitesetup/Resources/Private/Language/locallang_rte.xlf:classes.text-left
  text-right.name = LLL:EXT:sitesetup/Resources/Private/Language/locallang_rte.xlf:classes.text-right
  text-center.name = LLL:EXT:sitesetup/Resources/Private/Language/locallang_rte.xlf:classes.text-center
}
