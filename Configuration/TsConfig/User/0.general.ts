# default rte configuration for all users
setup.default {
  thumbnailsByDefault = 1
  rteWidth = 80%
  edit_wideDocument = 1
  edit_RTE = 1
}

options {
  enableBookmarks = 0
  createFoldersInEB = 1
  file_list.enableDisplayBigControlPanel = activated
  file_list.enableDisplayThumbnails = activated
  # defaultUploadFolder = 1:Bilder/
}
