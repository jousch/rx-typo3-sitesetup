styles.content.imgtext.maxW = 1140
styles.content.imgtext.maxWInText = 1140
styles.content.imgtext.linkWrap.lightboxEnabled = 1

styles.content.imgtext.colSpace = 10
styles.content.imgtext.rowSpace = 10
styles.content.imgtext.textMargin = 10
styles.content.imgtext.borderThick = 2