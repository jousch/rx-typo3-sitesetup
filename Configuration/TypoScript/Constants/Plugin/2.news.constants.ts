plugin.tx_news {
  settings.cssFile =
  opengraph.site_name = The Site
  rss.channel {
    title = The Site News
    link = http://www.example.com
    copyright = Reelworx GmbH
    language = en-gb
  }

  view {
    layoutRootPath = EXT:sitesetup/Resources/Private/Extensions/news/Layouts/
    partialRootPath = EXT:sitesetup/Resources/Private/Extensions/news/Partials/
    templateRootPath = EXT:sitesetup/Resources/Private/Extensions/news/Templates/
  }
}
