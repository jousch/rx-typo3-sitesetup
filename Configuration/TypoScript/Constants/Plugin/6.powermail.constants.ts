plugin.tx_powermail.settings {
  view {
    templateRootPath = EXT:sitesetup/Resources/Private/Extensions/powermail/Templates/
    partialRootPath = EXT:sitesetup/Resources/Private/Extensions/powermail/Layouts/
    layoutRootPath = EXT:sitesetup/Resources/Private/Extensions/powermail/Partials/
  }

  main {
    confirmation = 0
    optin = 0
    moresteps = 0
  }

  receiver.overwrite.returnPath = webmaster@example.com
  sender.overwrite.returnPath = webmaster@example.com
}
