plugin.tx_srlanguagemenu {
  doNotLinkInactive = 0
  showCurrentFirst = 1
  defaultCountryISOCode = GB
  defaultLanguageISOCode = EN
  defaultLayout = 2
  addLinksListHeader = 0

  view {
    layoutRootPath = EXT:sitesetup/Resources/Private/Extensions/sr_language_menu/Layouts/
    partialRootPath = EXT:sitesetup/Resources/Private/Extensions/sr_language_menu/Partials/
    templateRootPath = EXT:sitesetup/Resources/Private/Extensions/sr_language_menu/Templates/
  }
}
