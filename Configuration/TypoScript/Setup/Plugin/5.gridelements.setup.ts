# gridelements
tt_content.gridelements_pi1.20.10.setup {
  # ID of grid element
  1 < lib.gridelements.defaultGridSetup
  1 {
    cObject = FLUIDTEMPLATE
    cObject {
      file = {$resourcesPath}Private/Extensions/gridelements/twocol.html
    }
  }

  2 < lib.gridelements.defaultGridSetup
  2 {
    cObject = FLUIDTEMPLATE
    cObject {
      file = {$resourcesPath}Private/Extensions/gridelements/threecol.html
    }
  }
}
