#-- rtehtmlarea
lib.parseFunc_RTE {
  nonTypoTagStdWrap.encapsLines {
    addAttributes.P.class >
    remapTag.DIV >
  }

  # responsive tables
  externalBlocks := addToList(table)
  externalBlocks {
    table.stdWrap {
      wrap = <div class="table-responsive">|</div>
      HTMLparser.tags.table.fixAttrib.class {
        default = table
        list = table, table-bordered, table-condensed
      }
    }
  }

  # icons for li
  externalBlocks := addToList(ul,li)
  externalBlocks {
    ul.stripNL = 1
    ul.callRecursive = 1
    ul.callRecursive.tagStdWrap.HTMLparser = 1
    ul.callRecursive.tagStdWrap.HTMLparser.tags.ul {
      fixAttrib.class.default = icon-list
    }
  }
}

# Use Layout property of content to select a slider layout
lib.stdheader.10.1.dataWrap {
  override.cObject = TEXT
  override.cObject.value = <h1 class="slider collapsed" data-toggle="collapse" data-target="#c{field:uid} .collapse"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-right"></span>|</h1>
  override.cObject.insertData = 1
  override.if.value.field = layout
  override.if.equals = 1
}

lib.stdheader.10.2.dataWrap {
  override.cObject = TEXT
  override.cObject.value = <h2 class="slider collapsed" data-toggle="collapse" data-target="#c{field:uid} .collapse"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-right"></span>|</h2>
  override.cObject.insertData = 1
  override.if.value.field = layout
  override.if.equals = 1
}

lib.stdheader.10.3.dataWrap {
  override.cObject = TEXT
  override.cObject.value = <h3 class="slider collapsed" data-toggle="collapse" data-target="#c{field:uid} .collapse"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-right"></span>|</h3>
  override.cObject.insertData = 1
  override.if.value.field = layout
  override.if.equals = 1
}

tt_content.text.20.stdWrap {
  outerWrap = <div class="collapse">|</div>
  outerWrap.if.value.field = layout
  outerWrap.if.equals = 1
}

tt_content.image.20.stdWrap.outerWrap < tt_content.text.20.stdWrap

#-- headers always on top of content
tt_content.textpic.10.if >
tt_content.textpic.20.text.10 >

# filelist rendering
tt_content.uploads {
  wrap = <div class="row"><div class="col-xs-12">|</div></div>
  20.stdWrap.dataWrap = <ul class="download-list csc-uploads csc-uploads-{field:layout}">|</ul>
  20.renderObj {
    10.file.width = 100
    20.wrap = <strong>|</strong>
    20.sorting.direction = desc
    21 = COA
    21.1 = TEXT
    21.1.value = <strong>Filesize</strong>
    21.1.value.de = <strong>Dateigröße</strong>
    21.2 < tt_content.uploads.20.renderObj.40
    21.2.wrap >
    21.wrap = <span class="pull-right">|</span>
    40 >
    15 >
  }
}

# responsive images
tt_content.image.20 {
  1.params = class="img-responsive"
  1.sourceCollection >
  # lightbox
  1.imageLinkWrap.linkParams.ATagParams.dataWrap = data-gallery="{field:uid}" data-parent=".csc-textpic-imagewrap" data-toggle="lightbox" data-title="{file:current:title}" data-footer="{file:current:description}"

  # remove default centering code divs
  rendering.singleNoCaption.allStdWrap {
    innerWrap >
  }

  rendering.noCaption {
    rowStdWrap.wrap = <div class="row"> | </div>
    noRowsStdWrap.wrap = <div class="row csc-textpic-imagerow-none"> | </div>
    lastRowStdWrap.wrap = <div class="row csc-textpic-imagerow-last"> | </div>
    columnStdWrap.wrap =
    columnStdWrap.innerWrap.cObject = COA
    columnStdWrap.innerWrap.cObject {
      1 = TEXT
      1.value = <div class="col-sm-
      5 = TEXT
      5.value = 12
      5.stdWrap.dataWrap = | / {field:imagecols}
      5.prioriCalc = intval
      10 = TEXT
      10.value = ###CLASSES###"> | </div>
    }
  }

  # add class "row" on outer divs for center, right and left position
  layout {
    25.override = <div class="row csc-textpic csc-textpic-responsive csc-textpic-intext-right-nowrap###CLASSES###">###IMAGES######TEXT###</div>
    26.override = <div class="row csc-textpic csc-textpic-responsive csc-textpic-intext-left-nowrap###CLASSES###">###IMAGES######TEXT###</div>
  }
}

tt_content.textpic.20 {
  # set 50% width for text
  text {
    wrap >
    outerWrap.cObject = CASE
    outerWrap.cObject {
      key.field = imageorient
      # add pull/push classes if image position is right
      25 = TEXT
      25.value = <div class="col-sm-6 col-sm-pull-6"> | </div>
      26 = TEXT
      26.value = <div class="col-sm-6"> | </div>
      27 = TEXT
      27.value = <div class="col-sm-8"> | </div>
      28 = TEXT
      28.value = <div class="col-sm-4"> | </div>
      29 = TEXT
      29.value = <div class="col-sm-8 col-sm-pull-4"> | </div>
      30 = TEXT
      30.value = <div class="col-sm-4 col-sm-pull-8"> | </div>
    }
  }

  # set 50% width for image and text container
  rendering {
    singleNoCaption.allStdWrap {
      # we can't unset (>) dataWrap since it is only referenced
      dataWrap =
      dataWrap.override =
      dataWrap.cObject = CASE
      dataWrap.cObject {
        key.field = imageorient
        # add pull/push classes if image position is right
        25 = TEXT
        25.value = <div class="col-sm-6 col-sm-push-6" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
        26 = TEXT
        26.value = <div class="col-sm-6" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
        27 = TEXT
        27.value = <div class="col-sm-4" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
        28 = TEXT
        28.value = <div class="col-sm-8" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
        29 = TEXT
        29.value = <div class="col-sm-4 col-sm-push-8" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
        30 = TEXT
        30.value = <div class="col-sm-8 col-sm-push-4" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
      }
    }

    noCaption.allStdWrap {
      # we can't unset (>) dataWrap since it is only referenced
      dataWrap =
      dataWrap.override =
      dataWrap.cObject = CASE
      dataWrap.cObject {
        key.field = imageorient
        # add pull/push classes if image position is right
        25 = TEXT
        25.value = <div class="col-sm-6 col-sm-push-6" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
        26 = TEXT
        26.value = <div class="col-sm-6" data-csc-images="{register:imageCount}" data-csc-cols="{field:imagecols}"> | </div>
      }
    }
  }
}

# add new layout for carousel
temp.originalImage < tt_content.image.20

tt_content.image.20 >
tt_content.image.20 = CASE
tt_content.image.20 {
  key.field = layout
  default < temp.originalImage
  # carousel
  51 < temp.originalImage
}

tt_content.image.20.51 {
  renderMethod = none

  1.file.width >
  1.file.width = 1140
  #1.file.height = 300c

  layout >
  layout = TEXT
  layout.value = ###IMAGES######TEXT###

  oneImageStdWrap.outerWrap = <div class="item text-center">|</div>
  oneImageStdWrap.outerWrap.override = <div class="item text-center active">|</div>
  oneImageStdWrap.outerWrap.override.if.value.data = register:IMAGE_NUM
  oneImageStdWrap.outerWrap.override.if.equals = 0

  imgTagStdWrap.wrap = |<br />

  imageStdWrap >
  imageStdWrap.dataWrap (
      <div id="carousel{field:uid}" class="carousel slide" data-ride="carousel" data-interval="4000">
        <div class="carousel-inner">|</div>
        <a class="left carousel-control" href="#carousel{field:uid}" data-slide="prev"></a>
        <a class="right carousel-control" href="#carousel{field:uid}" data-slide="next"></a>
      </div>
  )
}

# responsive embedded videos
tt_content.media.20.mimeConf.swfobject.layout = <div class="embed-responsive embed-responsive-16by9">###SWFOBJECT###</div>
tt_content.media.20.mimeConf.swfobject.video.default.attributes.class = embed-responsive-item
