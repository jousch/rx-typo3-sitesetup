// set default extension name for labels in Fluid
lib.fluidContent.extbase.controllerExtensionName = {$extensionName}

// adjust default lightbox settings
lib.fluidContent.settings.media.popup.linkParams.ATagParams.dataWrap = data-gallery="{field:uid}" data-parent=".csc-textpic-imagewrap" data-toggle="lightbox" data-title="{file:current:title}" data-footer="{file:current:description}"
