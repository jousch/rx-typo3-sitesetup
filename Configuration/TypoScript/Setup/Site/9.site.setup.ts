// No indexing, we're not using a search
config.index_enable = 0

// don't show page name in page title on root page
[page | is_siteroot = 1]
  config.noPageTitle = 1
[end]

lib.mainContent < styles.content.get

lib.sidebar < styles.content.getLeft
lib.sidebar.slide = -1
lib.sidebar.slide.collect = -1
lib.sidebar.slide.collectReverse = 1

page = PAGE
page.typeNum = 0

page.meta {
  viewport = width=device-width, initial-scale=1.0
  X-UA-Compatible = IE=edge
  X-UA-Compatible.httpEquivalent = 1
}

page.includeCSS {
  styles = {$resourcesPath}Public/Css/main.css
}

page.includeJSFooter {
  mainjs = {$resourcesPath}Public/JavaScript/main.js
}

page.shortcutIcon = {$resourcesPath}Public/Images/favicon.png

page.10 = FLUIDTEMPLATE
page.10 {
  extbase.controllerExtensionName = {$extensionName}

  templateRootPaths.10 = {$resourcesPath}Private/Templates/
  layoutRootPaths.10 = {$resourcesPath}Private/Layouts/
  partialRootPaths.10 = {$resourcesPath}Private/Partials/

  # select template based on backend layout
  templateName = dummy value needed for stdWrap to be applied, bug! http://forge.typo3.org/issues/71113
  templateName.stdWrap.cObject = CASE
  templateName.stdWrap.cObject {
    key.data = pagelayout

    default = TEXT
    default.value = Subpage

    1 = TEXT
    1.value = Index
  }

  settings {
    rootPid = {$rootPageId}
  }
}

page.1001 =< plugin.tx_rlmplanguagedetection_pi1
