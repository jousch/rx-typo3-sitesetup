# General config
config {
  linkVars = L(0-1)
  sys_language_mode = content_fallback; 0
  sys_language_overlay = 1
}

# English
config {
  locale_all = en_US.utf8
  language = en
  sys_language_uid = 0
  sys_language_isocode_default = en
}

page.meta {
  page-topic = The Site
}

# German
[globalVar = GP:L = 1]
  config {
    locale_all = de_AT.utf8
    language = de
    sys_language_uid = 1
  }

  page.meta {
    page-topic = Die Seite
  }
[global]

lib.langMenu < plugin.tx_srlanguagemenu.widgets.menu
