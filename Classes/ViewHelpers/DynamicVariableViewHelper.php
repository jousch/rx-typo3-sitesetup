<?php
namespace Reelworx\Sitesetup\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @author Markus Klein <markus.klein@reelworx.at>
 */
class DynamicVariableViewHelper extends AbstractViewHelper
{

    /**
     * @param array $array
     * @param string $name
     *
     * @return string
     */
    public function render($array, $name)
    {
        return $array[$name];
    }

}
