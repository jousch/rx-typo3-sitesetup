Information
===========

Template sitesetup (theming, bootstrap, you name it) extension for TYPO3 CMS 7.
This extension provides all tools to set up a complete website.

We use `gulp` to generate CSS and javascript.

How to build things
-------------------

* Install `node` and `npm`
* Run `npm install && bower install` in `Build` folder
* Run `gulp help`

dd_googlesitemap rewrite rule
-----------------------------

`RewriteRule sitemap.xml$ /index.php?eID=dd_googlesitemap [L,R=301]`
