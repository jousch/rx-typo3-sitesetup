<?php

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Site Setup',
	'description' => 'Complete setup of a website. Includes all TypoScript, TSconfig and resources.',
	'category' => 'plugin',
	'version' => '1.0.0',
	'state' => 'stable',
	'uploadfolder' => 0,
	'author' => 'Markus Klein',
	'author_email' => 'office@reelworx.at',
	'author_company' => 'Reelworx GmbH',
	'constraints' => array (
		'depends' => array (
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array (
		),
		'suggests' => array (
            'fluid_styled_content' => '7.6.0-7.99.99',
			'gridelements' => '4.0.0-0.0.0',
			'typo3_console' => '1.2.0-0.0.0',
			'realurl' => '2.0.1-0.0.0',
			'news' => '3.2.2-0.0.0',
			'sr_language_menu' => '6.3.0-6.3.99',
			'rlmp_language_detection' => '7.0.0-7.99.99',
			'dd_googlesitemap' => '',
			'powermail' => '2.15.0-2.99.99',
			'yag' => '4.0.6-4.99.99',
			'rx_shariff' => '3.0.0-3.99.99'
		),
	),
);
