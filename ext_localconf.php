<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
	function($extKey)
	{
		$settings = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extKey]);

		if (!empty($settings['setPageTSconfig'])) {
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extKey . '/Configuration/TsConfig/Page/" extensions="ts">');
		}

		if (!empty($settings['setUserTSconfig'])) {
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extKey . '/Configuration/TsConfig/User/" extensions="ts">');
		}

		// configure realurl
		$realUrlConfigPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($extKey) . 'Configuration/realurl_conf.php';
		$realUrlConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['realurl']);
		if ($realUrlConfig['configFile'] !== $realUrlConfigPath) {
			$realUrlConfig['configFile'] = $realUrlConfigPath;
			$realUrlConfig['enableAutoConf'] = 0;

			/** @var \TYPO3\CMS\Core\Configuration\ConfigurationManager $configManager */
			$configManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ConfigurationManager::class);
			$configManager->setLocalConfigurationValueByPath('EXT/extConf/realurl', serialize($realUrlConfig));
		}
	},
	$_EXTKEY
);
