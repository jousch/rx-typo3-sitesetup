'use strict';

// -------------------------
var extKey = 'complex_extension-key'; // extension key (extension folder name)
var extName = 'ComplexExtension-key'; // extbase extension name
// -------------------------

var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');

var basePath = '../Resources/Public/';
var bootstrap_js = 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/';
var jquery_js = 'bower_components/jquery/dist/';

var scss_paths = [
	'bower_components/bootstrap-sass/assets/stylesheets/'
];

var cssPath = basePath + 'Css/';
var cssSource = cssPath + 'Scss/*.scss';

var jsPath = basePath + 'JavaScript/';
var jsDestName = 'main.js';
var jsSource = [
	jquery_js + 'jquery.js',
	bootstrap_js + 'transition.js',
	bootstrap_js + 'carousel.js',
	bootstrap_js + 'modal.js',
	bootstrap_js + 'dropdown.js',
	jsPath + 'src/*.js'
];

gulp.task('sass', function () {
	gulp.src(cssSource)
		.pipe(sass({
			includePaths: scss_paths
		}).on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(cssPath));
});

gulp.task('sass-release', function () {
	gulp.src(cssSource)
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths: scss_paths
		}).on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(cssPath));
});

gulp.task('js', function () {
	gulp.src(jsSource)
		.pipe(concat(jsDestName))
		.pipe(gulp.dest(jsPath));
});

gulp.task('js-release', function () {
	gulp.src(jsSource)
		.pipe(uglify())
		.pipe(concat(jsDestName))
		.pipe(gulp.dest(jsPath));
});

gulp.task('release', ['sass-release', 'js-release']);

gulp.task('watch', function() {
	gulp.watch(cssSource, ['sass']);
	gulp.watch(jsSource, ['js']);
});

gulp.task('create', function() {
	var replace = require('gulp-replace');
	var tmplateExtKey = 'sitesetup';
	var tmplateExtName = 'Sitesetup';
	gulp.src('../**/*.{xlf,php,ts,html,json}')
		.pipe(replace(tmplateExtKey, extKey, {skipBinary: true}))
		.pipe(replace(tmplateExtName, extName, {skipBinary: true}))
		.pipe(gulp.dest('../'));
});

gulp.task('help', function() {
	process.stdout.write('\n'
		+ '===========================================\n'
		+ 'Tasks: sass, js, sass-release, js-release, release, watch, create\n'
		+ '\n'
		+ 'Use "create" to rename sitesetup for a new project.\n'
		+ 'Update extKey and extName in gulpfile.js first.\n'
		+ '===========================================\n'
		+ '\n'
	);
});

gulp.task('default', ['sass', 'js']);
